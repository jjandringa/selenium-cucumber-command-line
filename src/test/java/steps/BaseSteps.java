/*
 * De hier gedefinieerde methoden worden voor elk scenario uitgevoerd.
 * De methode met de annotatie @Before wordt vooraf uitgevoerd,
 * de methode met de annotatie @After wordt na een scenario uitgevoerd.
 */
package steps;

import initialization.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author handringa
 */
public class BaseSteps {

    private static WebDriver driver = null;

    @Before
    public void beforeScenario() {
        // Algemene acties die voorafgaand aan elk scenario worden uitgevoerd.
        driver = new DriverFactory().getDriver();
    }

    @After
    public void afterScenario() throws Throwable {
        // Algemene acties die na afloop van een scenario worden uitgevoerd, ook bij falen.
        new DriverFactory().destroyDriver();
    }

}
