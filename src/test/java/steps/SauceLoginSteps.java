/*
 * Definieert de scenario stappen voor inloggen met de code om ze uit te voeren.
 * Per stap is er één methode met de annotatie @Given, @When of @Then gegeven.
 * De tekst bij de annotatie verwijst naar de stap in de feature file.
 */
package steps;

import initialization.DriverFactory;
import static org.assertj.core.api.Assertions.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.LoginPage;
import pages.Menubar;
import pages.ProductsPage;

/**
 *
 * @author handringa
 */
public class SauceLoginSteps extends DriverFactory {

    @Given("^Ik ben op de inlog pagina van de sauce demo website$")
    public void ik_ben_op_de_inlog_pagina_van_de_sauce_demo_website() throws Throwable {
        // Aanroep van een methode in LoginPage.java voor navigeren naar het inlogscherm.
        new LoginPage(driver).openSauceDemoWebsite();
    }

    @When("^Ik login met een geblokkeerd account$")
    public void ik_login_met_een_geblokkeerd_account() throws Throwable {
        // Het inloggen bestaat uit 3 acties:
        // - aanroepen van een methode in LoginPage.java voor invullen van de username
        // - aanroepen van een methode in LoginPage.java voor invullen van het password
        // - aanroepen van een methode in LoginPage.java voor het klikken op de login knop
        new LoginPage(driver).vulVeldUsername("locked_out_user");
        new LoginPage(driver).vulVeldPassword("secret_sauce");
        new LoginPage(driver).klikKnopLogin();
    }

    @Then("^Ik zou een foutmelding moeten zien$")
    public void ik_zou_een_foutmelding_moeten_zien() throws Throwable {
        // De controle bestaat uit 2 acties:
        // - aanroepen van een methode die de foutmelding van het scherm leest
        // - controleren of de juiste foutmelding wordt getoond
        String foutmelding = new LoginPage(driver).leesVeldError();
        assertThat(foutmelding).isEqualTo("Epic sadface: Sorry, this user has been locked out.");
    }

    @When("^Ik login met username \"([^\"]*)\"$")
    public void ik_login_met_username(String gebruikersnaam) throws Throwable {
        // Het inloggen bestaat uit 3 acties:
        // - aanroepen van een methode in LoginPage.java voor invullen van de username
        // - aanroepen van een methode in LoginPage.java voor invullen van het password
        // - aanroepen van een methode in LoginPage.java voor het klikken op de login knop
        new LoginPage(driver).vulVeldUsername(gebruikersnaam);
        new LoginPage(driver).vulVeldPassword("secret_sauce");
        new LoginPage(driver).klikKnopLogin();
    }

    @Then("^Ik zou de foutmelding \"([^\"]*)\" moeten zien$")
    public void ik_zou_de_foutmelding_moeten_zien(String verwacht) throws Throwable {
        // De controle bestaat uit 2 acties:
        // - aanroepen van een methode die de foutmelding van het scherm leest
        // - controleren of de juiste foutmelding wordt getoond
        String werkelijk = new LoginPage(driver).leesVeldError();
        assertThat(werkelijk).isEqualTo(verwacht);
    }

        @When("^Ik login met geldige credentials$")
    public void ik_login_met_geldige_credentials() throws Throwable {
        // Het inloggen bestaat uit 3 acties:
        // - aanroepen van een methode in LoginPage.java voor invullen van de username
        // - aanroepen van een methode in LoginPage.java voor invullen van het password
        // - aanroepen van een methode in LoginPage.java voor het klikken op de login knop
        new LoginPage(driver).vulVeldUsername("standard_user");
        new LoginPage(driver).vulVeldPassword("secret_sauce");
        new LoginPage(driver).klikKnopLogin();
    }

    @Then("^Ik zou de producten pagina moeten zien$")
    public void ik_zou_de_producten_pagina_moeten_zien() throws Throwable {
        // De controle bestaat uit 2 acties:
        // - lezen van een identificerend kenmerk op de producten pagina
        // - controleren of het kenmerk de juiste waarde heeft
        String product_label = new ProductsPage(driver).leesVeldProductLabel();
        assertThat(product_label).isEqualTo("Products");
    }

    @Then("^Ik zou moeten kunnen uitloggen$")
    public void ik_zou_moeten_kunnen_uitloggen() throws Throwable {
        // Om uit te loggen zijn 2 acties nodig:
        // - klikken op de knop voor het openen van het menu
        // - klikken op het menu-item voor uitloggen
        new Menubar(driver).klikKnopOpenMenu();
        new Menubar(driver).klikLinkLogout();
    }
    
}
