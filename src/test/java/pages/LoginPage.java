/*
 * Voor elke pagina in de user interface definieren we een java klasse.
 * Deze java klasse definieert de schermelementen als private variabelen
 * en public methoden voor de interactie met de elementen.
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author handringa
 */
public class LoginPage {

    WebDriver driver;

    public LoginPage(WebDriver driver) {
        // De constructor initialiseert de lokale driver en schermelementen.
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

//==============================================================================
    // Definitie van de schermelementen.
    //==============================================================================

    @FindBy(id = "user-name")
    private WebElement veldUsername;

    @FindBy(id = "password")
    private WebElement veldPassword;

    @FindBy(className = "btn_action")
    private WebElement knopLogin;

    @FindBy(css = "h3[data-test=error]")
    private WebElement veldError;
        
    //==============================================================================
    // Definitie van methoden voor interactie met de schermelementen.
    //==============================================================================
    
    public void openSauceDemoWebsite() {
        // Navigeer in de browser naar de login pagina.
        driver.get("https://www.saucedemo.com/");
    }

    public void vulVeldUsername(String username) {
        // Vul de username in.
        veldUsername.sendKeys(username);
    }

    public void vulVeldPassword(String password) {
        // Vul password in.
        veldPassword.sendKeys(password);
    }
    
    public void klikKnopLogin() {
        // Klik op de knop voor inloggen.
        knopLogin.click();
    }

    public String leesVeldError() {
        // Lees een getoonde foutmelding van het scherm.
        return veldError.getText();
    }

}
