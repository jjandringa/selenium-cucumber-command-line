/*
 * Voor elke pagina in de user interface definieren we een java klasse.
 * Deze java klasse definieert de schermelementen als private variabelen
 * en public methoden voor de interactie met de elementen.
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author handringa
 */
public class Menubar {

    WebDriver driver;

    public Menubar(WebDriver driver) {
        // De constructor initialiseert de lokale driver en schermelementen.
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definitie van de schermelementen.
    //==============================================================================
    
    @FindBy(xpath = "//button[text()=\"Open Menu\"]")
    private WebElement knopOpenMenu;

    @FindBy(id = "logout_sidebar_link")
    private WebElement linkLogout;
    
    //==============================================================================
    // Definitie van methoden voor interactie met de schermelementen.
    //==============================================================================

    public void klikKnopOpenMenu() {
        // Klik op de knop om het menu ye openen.
        knopOpenMenu.click();
    }

    public void klikLinkLogout() {
        // Selecteer de menu optie voor uitloggen.
        linkLogout.click();
    }


}
