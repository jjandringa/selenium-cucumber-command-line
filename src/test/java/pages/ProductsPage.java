/*
 * Voor elke pagina in de user interface definieren we een java klasse.
 * Deze java klasse definieert de schermelementen als private variabelen
 * en public methoden voor de interactie met de elementen.
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author handringa
 */
public class ProductsPage {

    WebDriver driver;

    public ProductsPage(WebDriver driver) {
        // De constructor initialiseert de lokale driver en schermelementen.
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definitie van de schermelementen.
    //==============================================================================
    
    @FindBy(className = "product_label")
    private WebElement veldProductLabel;
    
    //==============================================================================
    // Definitie van methoden voor interactie met de schermelementen.
    //==============================================================================

    public String leesVeldProductLabel() {
        // Lees een voor deze pagina identificerend kenmerk van het scherm.
        return veldProductLabel.getText();
    }

    
    

}
