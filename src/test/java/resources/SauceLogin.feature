@run
Feature: Inloggen op de sauce demo website

  Scenario Outline: Inloggen met ongeldig account
    Given Ik ben op de inlog pagina van de sauce demo website
    When Ik login met username "<Username>"
    Then Ik zou de foutmelding "<Foutmelding>" moeten zien

  Examples:
    | Username        | Foutmelding                                                               |
    | locked_out_user | Epic sadface: Sorry, this user has been locked out.                       |

  @fault
  Examples:
    | Username        | Foutmelding                                                               |
    | unknown_user    | Epic sadface: Username and password do not match any user in this service |

  @smoke
  Scenario: Inloggen met een geldig account
    Given Ik ben op de inlog pagina van de sauce demo website
    When Ik login met geldige credentials
    Then Ik zou de producten pagina moeten zien
    And Ik zou moeten kunnen uitloggen
