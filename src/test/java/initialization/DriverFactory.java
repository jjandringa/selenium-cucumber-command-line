/*
 * Definieert een driver variabele en methoden voor initialiseren en opruimen van de driver.
 * Initialiseert de driver bij de start van een scenario.
 * 
 */
package initialization;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author handringa
 */
public class DriverFactory {

    protected static WebDriver driver = null;

    public DriverFactory() {
        initialize();
    }

    private void initialize() {
        // Start de driver en impliciet ook de browser, Firefox in dit geval.
        if (driver == null) {
            System.setProperty("webdriver.gecko.driver", "geckodriver");
            createNewDriverInstance();
        }
    }

    private void createNewDriverInstance() {
        driver = new FirefoxDriver();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void destroyDriver() {
        // Stopt de browser en driver.
        driver.quit();
        driver = null;
    }

}
