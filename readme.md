# Selenium Cucumber Commandline
Testuitvoering starten vanaf de prompt. Hierbij wordt gebruik gemaakt van Apache maven.
Bijvoorbeeld te gebruiken in een CI/CD pipeline.

## Windows 10
Het pad naar mvn in het eeste deel van de instructie dient te worden aangepast aan de lokale situatie.
Het pad kan worden weggelaten wanneer het is voorgedefinieerd.

"C:\Program Files\netbeans\java\maven\bin\mvn.cmd" exec:java -Dexec.classpathScope=test^
 -Dexec.mainClass=io.cucumber.core.cli.Main -Dexec.args="src/test/java/resources
